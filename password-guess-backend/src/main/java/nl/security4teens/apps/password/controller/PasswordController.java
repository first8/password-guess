package nl.security4teens.apps.password.controller;

import lombok.extern.slf4j.Slf4j;
import nl.security4teens.apps.password.PasswordGuessApplication;
import nl.security4teens.apps.password.Request;
import nl.security4teens.apps.password.domain.response.Ok;
import nl.security4teens.apps.password.domain.response.Response;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "password")
@CrossOrigin(origins = {"guess.security4teens.nl", "http://guess.security4teens.nl", "https://guess.security4teens.nl"})
@Slf4j
public class PasswordController {

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Map<String, Integer> listPasswords() {
        return PasswordGuessApplication.passwords;
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void resetPasswords() {
        PasswordGuessApplication.guessedPasswords = new ArrayList<>();
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> statusGuesses() {
        return PasswordGuessApplication.guessedPasswords;
    }

    @RequestMapping(value = "/guess/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response guessPasswordPost(@RequestBody Request req) {
        return checkPassword(req.getPassword());
    }

    @RequestMapping(value = "/guess/{password}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response guessPassword(@PathVariable("password") String password) {
        return checkPassword(password);
    }

    private Ok checkPassword(String password) {
        Integer prevPosition = PasswordGuessApplication.passwords.get(password);
        if (prevPosition != null && !PasswordGuessApplication.guessedPasswords.contains(password)) {
            PasswordGuessApplication.guessedPasswords.add(password);
        }
        return Ok.builder()
                .found((prevPosition != null))
                .password(password)
                .previous((prevPosition != null) ? prevPosition : -1)
                .build();
    }

}
