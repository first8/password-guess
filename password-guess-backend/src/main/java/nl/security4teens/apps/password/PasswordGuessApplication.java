package nl.security4teens.apps.password;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@Slf4j
public class PasswordGuessApplication {
    public static Map<String, Integer> passwords = new HashMap<>();
    public static List<String> guessedPasswords = new ArrayList<>();

    public static void main(String[] args) {
        initPasswords();
        SpringApplication.run(PasswordGuessApplication.class, args);
    }

    private static void initPasswords() {
        InputStream inputStream = PasswordGuessApplication.class.getResourceAsStream("/passwords");
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                PasswordGuessApplication.passwords.put(parts[1], Integer.valueOf(parts[2]));
            }
        } catch (IOException e) {
            log.error("Error reading password list from passwords.", e);
        }
    }

}
