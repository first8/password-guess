package nl.security4teens.apps.password.domain.response;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Ok implements Response {
    private boolean found;
    private String password;
    private int previous;
}
