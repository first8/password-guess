<?php
    if(($_POST['username-email'] == 'k.mitnick@thehackacademy.nl') && ($_POST['password'] == 'SecureUnfriending')) {
        die('Antwoord van deze opdracht is: condor@thehackacademy.nl');
    }
?>
<html>
    <head>
        <title>The Hackademy - Webmail</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="main.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>
    <body>
   <div class="container">
       <div class="row">
           <div class='col-md-3'></div>
           <div class="col-md-6">
               <div class="login-box well">
                   <form method="post">
                       <legend>Sign In</legend>
                       <div class="form-group">
                       <label for="username-email">E-mail or Username</label>
                           <input name="username-email" id="username-email" placeholder="E-mail or Username" type="text" class="form-control" />
                           <?php if(($_POST['submit'] != null) && ($_POST['username-email'] != 'k.mitnick@thehackacademy.nl')) { ?>
                           <div class="alert alert-danger" role="alert">
                                <strong>Oh snap!</strong> Username unknown, please check and enter correct one.
                           </div>
                           <?php } ?>
                       </div>
                       <div class="form-group">
                           <label for="password">Password</label>
                           <input name="password" id="password" placeholder="Password" type="password" class="form-control" />
                           <?php if(($_POST['submit'] != null) && ($_POST['password'] != 'SecureUnfriending')) { ?>
                           <div class="alert alert-danger" role="alert">
                                <strong>Oh snap!</strong> Illegal password, please try again.
                           </div>
                           <?php } ?>
                       </div>
                       <div class="input-group">
                         <div class="checkbox">
                           <label>
                             <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                           </label>
                         </div>
                       </div>
                       <div class="form-group">
                           <input name="submit" type="submit" class="btn btn-default btn-login-submit btn-block m-t-md" value="Login" />
                       </div>
                       <span class='text-center'><a href="#" class="text-sm">Forgot Password?</a></span>
                   </form>

               </div>
           </div>
           <div class='col-md-3'></div>
       </div>
   </div>
    </body>
</html>