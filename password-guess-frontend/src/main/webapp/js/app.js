'use strict';

(function () {
    function init() {
        var router = new Router([
            new Route('home', 'home.html', readyHome, true)
        ]);
    }

    init();

    function readyHome() {
        function doPoll() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var json = JSON.parse(this.responseText);
                    var text = "";
                    for(var i = 0; i < json.length; i++) {
                        text += json[i]+"<br/>";
                    }
                    document.getElementById('foundPasswords').innerHTML = text;
                }
            }
            xhttp.open('GET', 'http://backend.security4teens.nl/password/status', true);
            xhttp.send();

            setTimeout(doPoll, 2000)
        }

        document.getElementById("guess").onclick = function (event) {
            event.preventDefault();
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    var json = JSON.parse(this.responseText);
                    var element = document.getElementById("guessStatus");
                    if(json.found) {
                        element.innerHTML = password + " gevonden";
                        element.className = "alert alert-success";
                    } else {
                        element.innerHTML = password + " niet gevonden";
                        element.className = "alert alert-danger";
                    }
                    document.getElementById("password").value = "";
                }
            };
            var password = document.getElementById("password").value;
            xhttp.open('POST', 'http://backend.security4teens.nl/password/guess/', true);
            xhttp.setRequestHeader("Content-type", "application/json");
            xhttp.send(JSON.stringify({'password': password}));
        };

        doPoll();
    }
}());
